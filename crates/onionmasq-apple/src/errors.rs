//! Error handling, and communicating failures across the JNI boundary.

use std::fmt::{Display, Formatter};

/// An error for 'the proxy was not running but you tried to interact with it like it was'.
///
/// This exists so we can propagate this case back to the C side, since it might be important:
/// we want C to be aware that (for example) they failed to refresh circuits or whatever.
#[derive(Debug)]
pub struct ProxyStoppedError;

impl Display for ProxyStoppedError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Attempted to do something with a stopped proxy")
    }
}

impl std::error::Error for ProxyStoppedError {}

/// An error for 'the country code you tried to use is invalid'.
///
/// This exists so we can propagate this case back to the C side.
#[derive(Debug)]
pub struct CountryCodeError {
    pub(crate) code: String,
}

impl Display for CountryCodeError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "Country code is invalid: {}", self.code)
    }
}

impl std::error::Error for CountryCodeError {}
